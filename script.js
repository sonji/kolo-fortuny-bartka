var solutions = ["Raz kozie śmierć", "Dla chcącego nic trudnego"];
var points = [0, 50, 100, 150, 200, 500];

var gameStatus = {
    wrongGuess: 0,
    maxWrongGuess: 4,
    wrongSolution: 0,
    maxWrongSolution: 2,
    currentSolution: ''
}

var gameElements = {
    board: $('.js-board'),
    letterExample: $('.js-letter'),
    statusBox: $('.js-status')
}

function drawLetter(letter) {
    var newLetter = gameElements.letterExample.clone();
    newLetter.text(letter);
    if (letter == ' ') {
        newLetter.addClass('space');
    }
    gameElements.board.append(newLetter);
}

function createSolution(solution) {
    gameElements.board.empty();
    var splittedSolution = solution.split('');
    for (var i = 0; i < splittedSolution.length; i++) {
        drawLetter(gameStatus.currentSolution[i]);
    }
}

function addMessage(msg) {
    gameElements.statusBox.prepend($('<br>'));
    gameElements.statusBox.prepend(msg);
}

function pickSolution(data) {
    var dataCount = data.length;
    var randomIndex = Math.floor(dataCount * Math.random());
    return data[randomIndex].toLowerCase();
}


function checkLetter(letter) {
    var letters = gameElements.board.children('.js-letter');

    var guessed = 0;
    var letterExists = false;

    letters.each(function () {
        if ($(this).text() === letter) {
            if ($(this).hasClass('folded')) {
                $(this).removeClass('folded');
                guessed++;
            }
            letterExists = true;
        }
    });


    if (letterExists === false) {
        gameStatus.wrongGuess++;
        var remainingGuesses = gameStatus.maxWrongGuess - gameStatus.wrongGuess;
        addMessage('Litera ' + letter + ' nie jest poprawna, masz jeszcze ' + remainingGuesses + ' mozliwosci');
        if (gameStatus.wrongGuess >= gameStatus.maxWrongGuess) {
            lose();
        }
    } else if (letterExists === true && guessed === 0) {
        addMessage('litera ' + letter + ' byla juz zgadnieta');
    } else {
        addMessage('Zgadles litere: ' + letter + '. Odslonieto ' + guessed + ' liter');
        if ($(".folded").length === countSpaces) {
            win();
        }
    }

}

function win() {
    addMessage("Wygrałeś!");
    gameElements.board.children('.js-letter:not(.space)').removeClass('folded');
    $('.js-solution, .js-input').attr('disabled', true);
}

function lose() {
    addMessage("Przegrałeś!");
    gameElements.board.children('.js-letter:not(.space)').removeClass('folded');
    $('.js-solution, .js-input').attr('disabled', true);
}

gameStatus.currentSolution = pickSolution(solutions);
createSolution(gameStatus.currentSolution);
var countSpaces = gameStatus.currentSolution.split(' ').length - 1;

$('.js-input').on('input', function () {
    var guessLetter = $(this).val().toLowerCase();
    $(this).val('');
    checkLetter(guessLetter);

});


$('.js-solution').on('change', function () {

    var guessedSolution = $(this).val().toLowerCase();
    if (guessedSolution === gameStatus.currentSolution) {
        win();

    } else {
        gameStatus.wrongSolution++;
        addMessage('Nie zgadłeś hasła :<');
        if (gameStatus.wrongSolution >= gameStatus.maxWrongSolution) {
            lose();
        }
    }

});
